package com.se214.apigateway.routes;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.Buildable;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;

@Configuration
public class UserRoute implements Router {
    @Value("${application.url.user-service}")
    private String userServiceUrl;
    @Value("${application.router.user-service}")
    private String userServiceRouter;

    @Override
    public Function<PredicateSpec, Buildable<Route>> route() {
        return route -> route
                .path(userServiceRouter)
                .filters(f -> f
                        .retry(3)
                )
                .uri(userServiceUrl);
    }
}
