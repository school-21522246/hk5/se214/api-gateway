package com.se214.apigateway.routes;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.builder.Buildable;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;

@Configuration
public class ProductRoute implements Router{
    @Value("${application.url.product-service}")
    private String proudctServiceUrl;
    @Value("${application.router.product-service}")
    private String productServiceRouter;

    @Override
    public Function<PredicateSpec, Buildable<Route>> route() {
        return route -> route
                .path(productServiceRouter)
                .filters(f -> f
                        .retry(3)
//                        .filter(authFilter.apply(new AuthFilter.Config(List.of("customer", "admin", "shop"))))
                )
                .uri(proudctServiceUrl);
    }
}
