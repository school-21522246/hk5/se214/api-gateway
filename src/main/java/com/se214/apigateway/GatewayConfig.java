package com.se214.apigateway;

import com.se214.apigateway.routes.ProductRoute;
import com.se214.apigateway.routes.UserRoute;
import lombok.AllArgsConstructor;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

@Configuration
@AllArgsConstructor
public class GatewayConfig {
    private final UserRoute userRoute;
    private final ProductRoute productRoute;

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(userRoute.getClass().getSimpleName(), userRoute.route())
                .route(productRoute.getClass().getSimpleName(), productRoute.route())
                .build();
    }

    @Bean
    public KeyResolver userKeyResolver() {
        return exchange -> Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
    }
}
