package com.se214.apigateway.filters;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class AuthFilter extends AbstractGatewayFilterFactory<AuthFilter.Config> {
    @Value("${application.url.user-service}")
    private String userServiceUrl;

    @Override
    public GatewayFilter apply(Config config) {
        return ((exchange, chain) -> {
            if (!exchange.getRequest().getHeaders().containsKey("Authorization")) {
                exchange.getResponse().setStatusCode(org.springframework.http.HttpStatus.UNAUTHORIZED);
                return exchange.getResponse().setComplete();
            }
            String authHeader = exchange.getRequest().getHeaders().get("Authorization").get(0);
            if (!authHeader.startsWith("Bearer ")) {
                exchange.getResponse().setStatusCode(org.springframework.http.HttpStatus.UNAUTHORIZED);
                return exchange.getResponse().setComplete();
            }
            String token = authHeader.substring(7);

            var restTemplate = new RestTemplate();
            try {
                String result = restTemplate.getForObject(userServiceUrl + "/validate-token?token=" + token, String.class);
                result = result.substring(1, result.length() - 1);

                if (!config.getRole().contains(result)) {
                    throw new Exception();
                }
            } catch (Exception e) {
                exchange.getResponse()
                        .setStatusCode(org.springframework.http.HttpStatus.UNAUTHORIZED);
                return exchange.getResponse().setComplete();
            }


            return chain.filter(exchange);
        });
    }

    @AllArgsConstructor
    @Getter
    public static class Config {
        private List<String> role;
    }
}
