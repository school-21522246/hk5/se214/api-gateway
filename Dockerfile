FROM eclipse-temurin:17-jre-alpine

EXPOSE 8080

COPY ./target/api-gateway-*.jar /usr/app/
WORKDIR /usr/app

CMD java -jar api-gateway-*.jar
